OBJ_FILES=src/main.o src/gol.o src/file_parser.o src/updater.o src/graphics.o

EXECUTABLE=gol
LDFLAGS=-lm -lSDL2
CFLAGS=-I./include -D_REENTRANT -I/usr/include/SDL2 -O3 -std=c99 # -DUSE_GRAPHICS -g 
CC=gcc

all: ${EXECUTABLE} compare

${EXECUTABLE}: ${OBJ_FILES}
	${CC} ${OBJ_FILES} -o ${EXECUTABLE} ${LDFLAGS}

.o: .c
	${CC} -c $@ -o $< ${CFLAGS}

compare: comparer/comparer.c
	${CC} comparer/comparer.c -o comparer/comparer -g

clean:
	rm -rf ${OBJ_FILES} ${EXECUTABLE} out.pbm comparer/comparer