#pragma once
#include "gol.h"

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>


/**
 * Loads the initial grid into the app state from a plain .pbm file.
 */
void load_grid(State *app_state);

void save_grid(State *app_state);