#pragma once


/**
 * Structure containing the global state of the program.
 */
typedef struct app_state_t
{
    unsigned int timesteps;
    char *input_filename;
    unsigned char *grid;
    unsigned int grid_width;
    unsigned int grid_height;
    unsigned int current_timestep;
} State;

/**
 * returns the main application state struct.
 */
State *get_app_state();

/**
 * Frees the allocated memmory of the state struct.
 */
void cleanup();

/**
 * Prints information of the current state including grid
 */
void print_state();