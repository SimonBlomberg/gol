#pragma once
#include <SDL2/SDL.h>
#include "gol.h"

void init_graphics(int w, int h, State *app_state);

void draw_state(State *app_state);

void destroy_graphics();