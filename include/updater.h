#pragma once
#include "gol.h"

/**
 * Performs one iteration of GoL on the State app_state
 */
void update_state(State *app_state);