#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "gol.h"
#include "file_parser.h"
#include "updater.h"

#ifdef USE_GRAPHICS
#include "graphics.h"
#endif

/**
 * Wall clock timing
 */
static double get_wall_seconds() 
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  double seconds = tv.tv_sec + (double)tv.tv_usec / 1000000;
  return seconds;
}

/**
 * Parses the command line and sets the appropriate settings in the state
 * structure.  
 */
void parse_cmd_args(int argc, char **argv, State *app_state)
{
    // Default parameters
    app_state->timesteps = 10;

    if(argc == 1)
    {
        printf("Usage: %s inputfile [timesteps]\n", argv[0]);
        exit(-1);
    }
    if(argc == 3)
    {
        app_state->timesteps = atoi(argv[2]);
    }

    char *filename = argv[1];

    app_state->input_filename = filename;
}


/**
 * Program takes input initial gol state and number of timesteps
 * as arguments.
 */
int main(int argc, char **argv)
{
    double start, end;
    State *app_state = get_app_state();
    parse_cmd_args(argc, argv, app_state);
    
    // Load initial state
    load_grid(app_state);

    // Initialize graphics
#ifdef USE_GRAPHICS
    init_graphics(1000, 1000, app_state);
#endif

    start = get_wall_seconds();
    while(app_state->current_timestep < app_state->timesteps)
    {
        update_state(app_state);
#ifdef USE_GRAPHICS
        draw_state(app_state);
#endif
    }
    end = get_wall_seconds();

    save_grid(app_state);
    printf("Time: %.4f s\n", end-start);

    // Free memory
#ifdef USE_GRAPHICS
    destroy_graphics();
#endif
    cleanup();
    return 0;
}