#include "graphics.h"

static SDL_Window *window;
static SDL_Renderer *renderer;
static int width;
static int height;
static int block_width, block_height;

void init_graphics(int w, int h, State *app_state)
{
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow("Game of Life", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
                                w, h, SDL_WINDOW_OPENGL);
    if(window == NULL)
    {
        printf("Could not create window: %s\n", SDL_GetError());
        exit(-1);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    
    if(renderer == NULL)
    {
        printf("Could not create renderer: %s\n", SDL_GetError());
        exit(-1);  
    }


    block_width = w / app_state->grid_width;
    block_height = h / app_state->grid_height;
}

static void draw_grid(State *app_state)
{
    for(int i = 0; i <= app_state->grid_height; i++)
    {
        for(int j = 0; j <= app_state->grid_width; j++)
        {
            SDL_Rect r = {i*block_height, j*block_width, block_width, block_height};

            if(app_state->grid[i*app_state->grid_width + j] == 1)
                SDL_RenderFillRect(renderer, &r);
        }
    }
}

void draw_state(State *app_state)
{
    SDL_SetRenderDrawColor(renderer, 40, 40, 40, 255);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 200, 200, 255, 255);
    draw_grid(app_state);

    SDL_RenderPresent(renderer);
    SDL_Delay(100);
}

void destroy_graphics()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}