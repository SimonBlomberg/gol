#include "gol.h"
#include <stdlib.h>
#include <stdio.h>
/// Main state of the application
static State m_state;

State *get_app_state()
{
    return &m_state;
}

void cleanup()
{
    free(m_state.grid);
}

void print_state()
{
    printf("Timestep %d\n", m_state.current_timestep);
    printf("Grid: \n");
    for(int i = 0; i < m_state.grid_height; i++)
    {
        for(int j = 0; j < m_state.grid_width; j++)
        {
            if(m_state.grid[i*m_state.grid_width + j] == 0)
                printf(" ");
            else
                printf("#");
        }
        printf("\n");
    }
}

