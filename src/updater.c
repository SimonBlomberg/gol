#include "updater.h"
#include <stdlib.h>

unsigned int get_neighbours(State *app_state, int i, int j)
{
    unsigned int ret = 0;
    for(int subi = -1; subi <= 1; subi++)
    {
        for(int subj = -1; subj <= 1; subj++)
        {
            int w = app_state->grid_width;
            int h = app_state->grid_height;
            int x = (j + subj) % w;
            int y = (i + subi) % h;

            x = x < 0 ? w + x : x;
            y = y < 0 ? h + y : y;

            if(!(subi == 0 && subj == 0))
                ret += app_state->grid[x + y*app_state->grid_width];
        }
    }
    return ret;
}


void update_state(State *app_state)
{
    char *next_grid = (char *)malloc(sizeof(char) * app_state->grid_width * app_state->grid_height);
    
    for(int i = 0; i < app_state->grid_height; i++)
    {
        for(int j = 0; j < app_state->grid_width; j++)
        {
            unsigned int nb = get_neighbours(app_state, i, j);
            if(nb == 3)
            {
                next_grid[i*app_state->grid_width + j] = 1;
            }
            else if(nb < 2 || nb > 3)
            {
                next_grid[i*app_state->grid_width + j] = 0;
            }
            else
            {
                next_grid[i*app_state->grid_width + j] = app_state->grid[i*app_state->grid_width + j];
            }
        }
    }



    // update current grid
    for(int i = 0; i < app_state->grid_width*app_state->grid_height; i++)
    {
        app_state->grid[i] = next_grid[i];
    }

    free(next_grid);
    // increase time
    app_state->current_timestep++;
}