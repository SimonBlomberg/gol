#include "file_parser.h"

void save_grid(State *app_state)
{
    FILE *outfile = fopen("out.pbm", "w");
    if(outfile == NULL)
    {
        printf("Error opening input file %d\n", errno);
        exit(-1);
    }

    fprintf(outfile, "P1\n#Test comment\n%d %d\n", app_state->grid_width, app_state->grid_height);
    for(int i = 0; i < app_state->grid_width * app_state->grid_height; i++)
    {
        // print newline every 70 characters
        if(!(i % 70) && i != 0)
            fprintf(outfile, "\n");

        if(app_state->grid[i] == 0)
            fprintf(outfile, "1");
        if(app_state->grid[i] == 1)
            fprintf(outfile, "0");
    }

    fclose(outfile);
}

void load_grid(State *app_state)
{
    char *magic = (char *)malloc(sizeof("P1"));
    int width;
    int height;

    FILE *infile = fopen(app_state->input_filename, "r");
    if(infile == NULL)
    {
        printf("Error opening input file %d\n", errno);
        exit(-1);
    }

    // Check magic number
    fread(magic, sizeof("P1"), 1, infile);
    if(strcmp("P1\n", magic) != 0)
    {
        printf("The input file does not have the plain ppm magic nr in it's header\n");
        exit(-1);
    }

    free(magic);
    char comment[255];
    char hash;
    fread(&hash, sizeof(char), 1, infile);
    if(hash == '#')
        // Comment from gimp
        fscanf(infile,"%[^\n]", comment);
    else
        fseek(infile, -1, SEEK_CUR);
    

    // width and height
    fscanf(infile, "%d %d", &width, &height);

    // Allocate map buffer
    app_state->grid = (unsigned char *)malloc(width*height*sizeof(unsigned char *));
    app_state->grid_width = width;
    app_state->grid_height = height;


    char in = '0';
    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            fread(&in, sizeof(char), 1, infile);

            // skip newlines
            if(in == '\n')
                fread(&in, sizeof(char), 1, infile); 
            if(in == '0')
                app_state->grid[i*width + j] = 1;
            else if(in == '1')
                app_state->grid[i*width + j] = 0;
            else
            {
                printf("Error reading input file, unrecognized cell: %c\n",in);
                exit(-1);
            }
            
        }
    }
    fclose(infile);
}