#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

void load_file(char *filename, unsigned char ** grid, int *buff_size)
{
    printf("%s:\n", filename);
    char *magic = (char *)malloc(sizeof("P1"));
    int width;
    int height;

    FILE *infile = fopen(filename, "r");
    if(infile == NULL)
    {
        printf("Error opening input file %d\n", errno);
        exit(-1);
    }

    // Check magic number
    fread(magic, sizeof("P1"), 1, infile);
    if(strcmp("P1\n", magic) != 0)
    {
        printf("%s\n", magic);
        printf("The input file does not have the plain ppm magic nr in it's header\n");
        exit(-1);
    }
    free(magic);

    char comment[255];
    char hash;
    fread(&hash, sizeof(char), 1, infile);
    if(hash == '#')
        // Comment from gimp
        fscanf(infile,"%[^\n]", comment);
    else
        fseek(infile, -1, SEEK_CUR);
    

    // width and height
    fscanf(infile, "%d %d", &width, &height);

    // Allocate map buffer
    *grid = (unsigned char *)malloc(width*height*sizeof(unsigned char *));
    *buff_size = width*height;

    char in = '0';
    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            fread(&in, sizeof(char), 1, infile);

            // skip newlines
            if(in == '\n')
                fread(&in, sizeof(char), 1, infile); 
            if(in == '0')
                (*grid)[i*width + j] = 1;
            else if(in == '1')
                (*grid)[i*width + j] = 0;
            else
            {
                printf("Error reading input file, unrecognized cell: %c\n",in);
                exit(-1);
            }
            
        }
    }
    fclose(infile);
}

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        printf("Usage: %s file1.pbm file2.pbm\n", argv[0]);
        exit(-1);
    }

    unsigned char *g1;
    unsigned char *g2;

    int s1, s2;
    load_file(argv[1], &g1, &s1);
    load_file(argv[2], &g2, &s2);

    for(int i = 0; i < s1; i++)
    {
        if(g1[i] != g2[i])
        {
            printf("Not equal!\n");
            return -1;
        }
    }
    printf("Equal!\n");

    free(g1);
    free(g2);
    return 0;
}